#pragma config(Sensor, dgtl1,  leftEncoder, sensorQuadEncoder)
#pragma config(Motor, port1, leftMotorf, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port2, leftMotorb, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port3, rightMotorb, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port4, rightMotorf, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port5, lift1, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port6, lift2, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port7, lift3, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port8, lift4, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port9, claw1, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port10, claw2, tmotorNormal, openLoop, reversed)
//Set motor Aliases

//constants
const int PULSESPERROT=360;
const int PULSEPER90DEG=30*PULSESPERROT;
const int PULSESPERCENTISECOND=30;

//settings
bool ENCODER_ON=true;

//implement settings
float pulses_val=0;
float *pulses=ENCODER_ON ? &SensorValue : &pulses_val;

//other variables
bool rot90=false;

//Sets all motors to given value
void set_motors(int v	alue) {
	motor[leftMotorf]=value;
	motor[leftMotorb]=value;
 	motor[rightMotorf]=value;
	motor[rightMotorb]=value;
}

void resetSensor() {
	if (ENCODER_ON) *pulses=0;
	else ClearTimer(T1);
}

void set_pulse() {
	pulses_val=(float) (time10[T1]*PULSESPERCENTISECOND);
}

void set_lift(int value) {
	motor[lift1]=value;
	motor[lift2]=value;
	motor[lift3]=-value;
	motor[lift4]=-value;

}

void move_claw(int dir) {
	motor[claw1]=127*dir;
	motor[claw2]=127*dir;
}

void turn(int dir) {
	motor[leftMotorf]=dir*127;
	motor[leftMotorb]=dir*127;
 	motor[rightMotorf]=dir*127*-1;
	motor[rightMotorb]=dir*127*-1;
}

//Gives joysticks values to motors
void check_joysticks() {
	motor[leftMotorf]=vexRT[Ch3];
	motor[leftMotorb]=vexRT[Ch3];
 	motor[rightMotorf]=vexRT[Ch2];
	motor[rightMotorb]=vexRT[Ch2];
}

//Checks if given full power button is pressed and sets motors
int check_full(int but, int dir) {
	 //if pressed then but*127*dir gives full power in desired direction
	if (but==1) set_motors(but*127*dir);
	return but; //returns but so no need to store but in temp var
}

int check_sharp(int but, int dir) {
	if (but==1) turn(dir);
	return but;
}

void check_claw() {
	if (vexRT[Btn7U]==1) move_claw(1);
	else if (vexRT[Btn7D]==1) move_claw(-1);
	else move_claw(0);

}

void check_lift() {
	if (vexRT[Btn6U]==1) set_lift(127);
	else if (vexRT[Btn6D]==1) set_lift(-127);
	else set_lift(0);
}

bool check_rot90() {
	if (check_sharp(vexRT[Btn8R], 1) || check_sharp(vexRT[Btn8L], -1)) {
		resetSensor();
		rot90=true;
		return true;
	}
	return false;
}

//main function
task main () {
	//main loop
	resetSensor();
	while (true) {
		if (!ENCODER_ON) set_pulse();
		//lift and claw fully independant
		check_lift();
		check_claw();
		//if full power buttons pressed, skip rest of the steps
		//move precedence is forward, backwards, left, right, joystick
		if (!rot90) {
			if (check_rot90()) continue;
			if (check_full(vexRT[Btn7U], 1)==1) continue;
			if (check_full(vexRT[Btn7D], -1)==1) continue;
			if (check_sharp(vexRT[Btn7R], 1)==1) continue;
			if (check_sharp(vexRT[Btn7L], -1)==1) continue;
			//if both not pressed set motor to joysticks
			check_joysticks();
		}
		else {
			if (*pulses >= PULSEPER90DEG) {
				set_motors(0);
				rot90=false;
			}
		}
	}
}
