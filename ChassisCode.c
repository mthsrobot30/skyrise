#pragma config(Motor, port1, leftMotorf, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port2, leftMotorb, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port3, rightMotorb, tmotorNormal, openLoop, reversed)
#pragma config(Motor, port4, rightMotorf, tmotorNormal, openLoop, reversed)
//Set motor Aliases

//Sets all motors to given value
void set_motors(int value) {
	motor[leftMotorf]=value;
	motor[leftMotorb]=value;
 	motor[rightMotorf]=value;
	motor[rightMotorb]=value;
}

void turn(int dir) {
	motor[leftMotorf]=dir*127;
	motor[leftMotorb]=dir*127;
 	motor[rightMotorf]=dir*127*-1;
	motor[rightMotorb]=dir*127*-1;
}

//Gives joysticks values to motors
void check_joysticks() {
	motor[leftMotorf]=vexRT[Ch3];
	motor[leftMotorb]=vexRT[Ch3];
 	motor[rightMotorf]=vexRT[Ch2];
	motor[rightMotorb]=vexRT[Ch2];
}

//Checks if given full power button is pressed and sets motors
int check_full(int but, int dir) {
	 //if pressed then but*127*dir gives full power in desired direction
	if (but==1) set_motors(but*127*dir);
	return but; //returns but so no need to store but in temp var
}

int check_sharp(int but, int dir) {
	if (but==1) turn(dir);
	return but;
}

//main function
task main () {
	//main loop
	while (true) {
		//if full power buttons pressed, skip rest of the steps
		//move precedence is forward, backwards, left, right, joystick
		if (check_full(vexRT[Btn7U], 1)==1) continue;
		if (check_full(vexRT[Btn7D], -1)==1) continue;
		if (check_sharp(vexRT[Btn7R], 1)==1) continue;
		if (check_sharp(vexRT[Btn7L], -1)==1) continue;
		//if both not pressed set motor to joysticks
		check_joysticks();
	}
}
